
dragon.png
size: 1024,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
back
  rotate: false
  xy: 2, 334
  size: 190, 185
  orig: 190, 185
  offset: 0, 0
  index: -1
chest
  rotate: false
  xy: 2, 697
  size: 136, 122
  orig: 136, 122
  offset: 0, 0
  index: -1
chin
  rotate: false
  xy: 611, 613
  size: 214, 146
  orig: 214, 146
  offset: 0, 0
  index: -1
front-toe-a
  rotate: true
  xy: 2, 914
  size: 29, 50
  orig: 29, 50
  offset: 0, 0
  index: -1
front-toe-b
  rotate: true
  xy: 467, 887
  size: 56, 57
  orig: 56, 57
  offset: 0, 0
  index: -1
left-front-leg
  rotate: false
  xy: 599, 886
  size: 84, 57
  orig: 84, 57
  offset: 0, 0
  index: -1
left-front-thigh
  rotate: false
  xy: 782, 871
  size: 84, 72
  orig: 84, 72
  offset: 0, 0
  index: -1
left-rear-leg
  rotate: false
  xy: 465, 434
  size: 206, 177
  orig: 206, 177
  offset: 0, 0
  index: -1
left-rear-thigh
  rotate: true
  xy: 216, 819
  size: 91, 149
  orig: 91, 149
  offset: 0, 0
  index: -1
left-wing01
  rotate: true
  xy: 403, 241
  size: 191, 256
  orig: 191, 256
  offset: 0, 0
  index: -1
left-wing02
  rotate: true
  xy: 673, 401
  size: 179, 269
  orig: 179, 269
  offset: 0, 0
  index: -1
left-wing03
  rotate: true
  xy: 194, 286
  size: 186, 207
  orig: 186, 207
  offset: 0, 0
  index: -1
left-wing04
  rotate: false
  xy: 140, 682
  size: 188, 135
  orig: 188, 135
  offset: 0, 0
  index: -1
left-wing06
  rotate: true
  xy: 661, 207
  size: 192, 331
  orig: 192, 331
  offset: 0, 0
  index: -1
left-wing07
  rotate: true
  xy: 2, 521
  size: 159, 255
  orig: 159, 255
  offset: 0, 0
  index: -1
left-wing08
  rotate: true
  xy: 827, 582
  size: 164, 181
  orig: 164, 181
  offset: 0, 0
  index: -1
left-wing09
  rotate: false
  xy: 259, 474
  size: 204, 167
  orig: 204, 167
  offset: 0, 0
  index: -1
right-front-leg
  rotate: false
  xy: 113, 821
  size: 101, 89
  orig: 101, 89
  offset: 0, 0
  index: -1
right-front-thigh
  rotate: false
  xy: 758, 761
  size: 108, 108
  orig: 108, 108
  offset: 0, 0
  index: -1
right-rear-leg
  rotate: false
  xy: 640, 773
  size: 116, 100
  orig: 116, 100
  offset: 0, 0
  index: -1
right-rear-thigh
  rotate: true
  xy: 367, 794
  size: 91, 149
  orig: 91, 149
  offset: 0, 0
  index: -1
right-rear-toe
  rotate: false
  xy: 2, 833
  size: 109, 77
  orig: 109, 77
  offset: 0, 0
  index: -1
right-wing02
  rotate: true
  xy: 635, 2
  size: 203, 305
  orig: 203, 305
  offset: 0, 0
  index: -1
right-wing04
  rotate: false
  xy: 330, 643
  size: 279, 144
  orig: 279, 144
  offset: 0, 0
  index: -1
right-wing06
  rotate: true
  xy: 2, 84
  size: 200, 366
  orig: 200, 366
  offset: 0, 0
  index: -1
right-wing07
  rotate: true
  xy: 370, 39
  size: 200, 263
  orig: 200, 263
  offset: 0, 0
  index: -1
tail01
  rotate: true
  xy: 868, 748
  size: 120, 153
  orig: 120, 153
  offset: 0, 0
  index: -1
tail02
  rotate: true
  xy: 518, 789
  size: 95, 120
  orig: 95, 120
  offset: 0, 0
  index: -1
tail03
  rotate: true
  xy: 868, 870
  size: 73, 92
  orig: 73, 92
  offset: 0, 0
  index: -1
tail04
  rotate: true
  xy: 526, 887
  size: 56, 71
  orig: 56, 71
  offset: 0, 0
  index: -1
tail05
  rotate: true
  xy: 406, 891
  size: 52, 59
  orig: 52, 59
  offset: 0, 0
  index: -1
tail06
  rotate: false
  xy: 685, 875
  size: 95, 68
  orig: 95, 68
  offset: 0, 0
  index: -1
thiagobrayner
  rotate: false
  xy: 54, 912
  size: 350, 31
  orig: 350, 31
  offset: 0, 0
  index: -1

dragon2.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
head
  rotate: true
  xy: 2, 214
  size: 296, 260
  orig: 296, 260
  offset: 0, 0
  index: -1
left-wing05
  rotate: true
  xy: 741, 39
  size: 218, 213
  orig: 218, 213
  offset: 0, 0
  index: -1
right-wing01
  rotate: false
  xy: 264, 200
  size: 219, 310
  orig: 219, 310
  offset: 0, 0
  index: -1
right-wing03
  rotate: true
  xy: 485, 238
  size: 272, 247
  orig: 272, 247
  offset: 0, 0
  index: -1
right-wing05
  rotate: true
  xy: 734, 259
  size: 251, 229
  orig: 251, 229
  offset: 0, 0
  index: -1
right-wing08
  rotate: true
  xy: 485, 2
  size: 234, 254
  orig: 234, 254
  offset: 0, 0
  index: -1
right-wing09
  rotate: false
  xy: 2, 8
  size: 248, 204
  orig: 248, 204
  offset: 0, 0
  index: -1
